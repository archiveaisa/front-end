import { MutationTree } from 'vuex';
import { DataState } from '../state/data.state';
import { Utils } from '../../model/helper/utils.helper';



namespace DataSetter {

  export function setters<T>() {
    const dataMutations: MutationTree<DataState<T>> = {
       loadData(state: DataState<T>, datas: Array<T>)  {
         state.error = false;
         state.data.datas = datas;
       },

       dataError(state) {
         state.error = true;
       },
       delete(state: DataState<T>, util: Utils<T>) {
         state.data.delete(util.item, util.index);
       },
       update(state: DataState<T>, util: Utils<T>) {
         state.data.update(util);
       },
       create(state: DataState<T>, user: T ) {
         state.data.add(user);
       }
   }
   return dataMutations;
  }


}

export { DataSetter }
