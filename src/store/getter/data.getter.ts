import { GetterTree } from "vuex";
import { DataState } from "../state/data.state";
import { RootState } from "../state/root.state";
import { Data } from "../../model/data/data.model";


namespace DataGetter {

  export function getters<T>() {
     const dataGetters: GetterTree<DataState<T>, RootState> = {
      data(state: DataState<T>): Data<T> {
        return  state.data;
      },
    }
    return dataGetters;
  }

}
export { DataGetter }
