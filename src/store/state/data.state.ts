import { Data } from "../../model/data/data.model";



interface DataState<T> {
    data: Data<T>,
    error: boolean
}

export { DataState }
