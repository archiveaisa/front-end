import { Module } from "vuex";

import { Data } from "../../model/data/data.model";
import { DataState } from "../state/data.state";
import { RootState } from "../state/root.state";
import { DataGetter } from "../getter/data.getter";
import { DataAction } from "../action/data.action";
import { DataSetter } from "../mutation/data.mutation";


const namespaced: boolean = true;
namespace DataModule   {

  function state<T>(type: new(data: T) => T) {
    const state: DataState<T> = {
         data: new Data<T>(type),
         error: false
     }
     return state;
  }
  export function modules<T>(url:string, type: new(data: T) => T) : Module<DataState<T>, RootState> {
    const DataModules: Module<DataState<T>, RootState> = {
      namespaced: namespaced,
      state : state(type),
      getters : DataGetter.getters<T>(),
      actions: DataAction.actions<T>(url),
      mutations: DataSetter.setters<T>()
    }
    return DataModules;
  }

}

export { DataModule }
