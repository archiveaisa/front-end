import Vue from "vue";
import Vuex, { StoreOptions } from "vuex"


import { DataModule } from "./module/data.module";
import { RootState } from "./state/root.state";



Vue.use(Vuex);

//const Users = DataModule.modules<User>("/user/", User);
// const Clients = DataModule.modules<Client>("/client/", Client);
// const Adresses = DataModule.modules<Adress>("/adress/", Adress);
const store: StoreOptions<RootState> = {
    state: {
        scheme: 'Bearer ' // a simple property
    },
    modules: {
    }
};

export default new Vuex.Store<RootState>(store);
