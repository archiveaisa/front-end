import { ActionTree, ActionContext } from 'vuex';
import { RootState } from '../state/root.state';
import { DataState } from '../state/data.state';
import { ApiService } from '../../services/api/api.service';
import { Utils } from '../../model/helper/utils.helper';




namespace DataAction {
  export function actions<T>(url: string) {
    const DataAction: ActionTree<DataState<T>, RootState> = {
      getAll({ commit }): any {
          return ApiService.getAll<T>(url).then((response) => {
              commit('loadData', response.data);
              console.log('recalll')
           }).catch((error) => {
              console.log('error ', error);
              commit('dataError', error.response.data);
              return Promise.reject(error);
           })
        },
        delete({commit}, util: Utils<T>) {
            return ApiService.deleteOne<T>(url, util.id).then((response) => {
              commit('delete', util)
            }).catch((error) => {
              commit('dataError', error.response.data);
              return Promise.reject(error);
            });
        },
         update({ commit }, util: Utils<T>) {
            return ApiService.update<T>(url + util.id, util.item).then((response) => {
                commit('update', util);
            })
            .catch((error) => {
              commit('dataError', error.response.data);
              return Promise.reject(error);
            })
        },
        create({commit}, item: T) {
          return ApiService.create<T>(url, item).then((response) => {
            commit('create', item);
          })
          .catch((error) => {
            commit('dataError', error.response.data);
            return Promise.reject(error);
          })
        }

    }
    return DataAction;
  }
}



export { DataAction, }
