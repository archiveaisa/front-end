import Axios, { AxiosRequestConfig, AxiosInstance, AxiosPromise } from "axios";



namespace ApiService {
  const baseURL: string = "http://localhost:4000";
  let requestConfig: AxiosRequestConfig = {
      baseURL: baseURL,
  };
  const repository: AxiosInstance = Axios.create(requestConfig);

  export function getAll<T>(url: string): AxiosPromise<T> {
    return repository.get<T>(url);
  }

  export function getOne<T>(url: string, id: number): AxiosPromise<T> {
    return repository.get<T>(url + id);
  }

  export function create<T>(url: string, newOne: T): AxiosPromise<T> {
    return repository.post<T>(url, newOne);
  }

  export function update<T>(url: string, newOne: T): AxiosPromise<T> {
    return repository.put<T>(url, newOne);
  }

  export function deleteOne<T>(url: string, id: number): AxiosPromise<T> {
    return repository.delete(url + id);
  }

  export function addToken(accessToken: string) {
    repository.defaults.headers.common["Authorization"] = "Bearer" + accessToken;
  }

}

export { ApiService }
