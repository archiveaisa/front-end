import { i18n } from "../../i18n/i18n";


namespace TranslateService {
  export  function getPATH(table: string, component: string){
    const PATH: string = table + "." + component;
    return PATH;
  }
}

export { TranslateService }
