class Record {
  private  id: number;

  constructor( id: number) {
    this.id = id;
  }

  get _id(): number {
    return this.id
  }

  set _id(id: number) {
    this.id = id;
  }

}

export { Record }
