import { Record } from "./record.helper";

class Person extends Record{
  private name: string;
  private first_name: string;
  private last_name: string;
  private email: string;
  private ref: string;
  private adressId: number;

  constructor (id: number, name : string,
               first_name: string, last_name: string,
               email: string, ref: string, adressId: number) {
      super(id);
      this.name = name;
      this.first_name = first_name;
      this.last_name = last_name;
      this.email = email;
      this.ref = ref;
      this.adressId = adressId;
  }

  public get _first_name(): string {
    return this.first_name;
  }

  public set _first_name(firstname: string) {
    this.first_name = firstname;
  }

  public get _email(): string {
    return this.email;
  }

  public set _email(email: string) {
    this.email = email;
  }

  public get _last_name(): string {
    return this.last_name;
  }

  public set _last_name(lastname: string) {
    this.last_name = lastname;
  }

  public get _ref(): string {
    return this.ref;
  }

  public set _ref(ref: string) {
    this.ref = ref;
  }

  public get _name(): string {
    return this.name;
  }

  public set _name(name: string) {
    this.name = name;
  }

  public get _adressId() : number {
    return this.adressId;
  }

  public set _adressId(adressId: number) {
    this.adressId = adressId;
  }
}

export { Person }
