import * as THREE from 'three';
import { Ring } from '../ring/ring.geometry';

class Earth {
    private _geometry: THREE.SphereGeometry;
    private _material  : THREE.MeshPhongMaterial;
    private _texture : THREE.TextureLoader;
    private _displacementMap: THREE.TextureLoader;
    private _earth : THREE.Mesh;
    private _group : THREE.Group;
    private _rings: Array<Ring>;
    private _colorsRing : any[] = [0xffffff, 0xffff00, 0xff0000, 0x0000ff, 0x008000]

    constructor() {
        this._geometry = new THREE.SphereGeometry(2,32,32);
        this._material = new THREE.MeshPhongMaterial();
        this._texture = new THREE.TextureLoader();
        this._displacementMap = new THREE.TextureLoader();
        this._earth = new THREE.Mesh(this._geometry, this._material);
        this._group = new THREE.Group();
        //load texture
        this._material.map = this._texture.load(require("../../../img/worldColour.5400x2700.jpg")); 
        this._material.displacementMap = this._displacementMap.load(require("../../../img/gebco_bathy.5400x2700_8bit.jpg"));
        this._material.displacementScale = 0
        //init Array of Ring
        this._rings = new Array();
        this.initRings();
        this.initAllRotation()
    }

    initRings() {
        let gap : number = .085;
        let offset: number =.015;
        let outerRadius: number = 2.2
        let innerRadius: number = outerRadius + gap;
        this._colorsRing.forEach((color:any, index:number) => {
            this._rings.push(new Ring(innerRadius, outerRadius, 128,
                 {color: color, side: THREE.DoubleSide, opacity: 0.2, transparent: true}))
            outerRadius = outerRadius + gap + offset;
            innerRadius = outerRadius + gap;
            // this.addMeshToGroup(this._rings[this._rings.length - 1].mesh);
        })
    }

    addRingsToGroup() {
        this._rings.forEach((ring:Ring) => {
            this._group.add(ring.mesh);
        })
    }

    addRingsToEarth() {
        this._rings.forEach((ring:Ring) => {
            this._earth.add(ring.mesh);
        })
    }

    addPeoplesToRing(ring: Ring) {
        (ring.mesh.material as THREE.Material).opacity = .7;
        ring.addPeoplesToRing(this, this._group);
    }

    removePeoplesFromRing(ring: Ring) {
        (ring.mesh.material as THREE.Material).opacity = .2;
        ring.removePeopleFromRing(this, this._group);
    }
    private initAllRotation() {
        this._group.rotation.x = -1.2;
        this._group.position.y = 0.4;
        this._earth.rotation.y = 4.3;
        this._earth.rotation.x = .5;
    }


    get geometry() {
        return this._geometry;
    }

    get material() {
        return this._material;
    }

    get texture() {
        return this._texture;
    }

    get mesh() {
        return this._earth;
    }

    get group(): THREE.Group {
        return this._group;
    }

    get rings() : Array<Ring> {
        return this._rings;
    }

   

    
}
export { Earth }