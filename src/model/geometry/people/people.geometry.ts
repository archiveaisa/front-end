import * as THREE from 'three';

class People {
    private _geometry : THREE.CircleGeometry;
    private _material : THREE.MeshPhongMaterial;
    private _texture : THREE.TextureLoader;
    private _people : THREE.Mesh;   

    constructor(radius: number, segments: number, parameters: any, imageURL: any) {
        this._geometry = new THREE.CircleGeometry(radius, segments);
        this._material = new THREE.MeshPhongMaterial(parameters);
        this._texture = new THREE.TextureLoader();
        this._material.map = this._texture.load(imageURL);
        this._people = new THREE.Mesh(this._geometry, this._material) ;
        this.initRotation();
    }

    initRotation() {
        this.mesh.rotation.x = 0;
        this.mesh.rotation.y = 1;
        this.mesh.rotation.z = 1.5;
    }

    get geometry(): THREE.CircleGeometry {
        return this._geometry;
    }

    get material(): THREE.MeshPhongMaterial {
        return this._material;
    }

    get texture(): THREE.TextureLoader {
        return this._texture;
    }

    get mesh(): THREE.Mesh {
        return this._people;
    }


}

export {
    People
}