import * as THREE from 'three';
import { Earth } from '../earth/earth.geometry';
import { People } from '../people/people.geometry';
class Ring {
    private _geometry : THREE.RingGeometry;
    private _material : THREE.MeshBasicMaterial;
    private _ring : THREE.Mesh;
    private _peoples: Array<People>;
    private _peopleData: Array<any> = [require("../../../img/alawi.jpg"), require("../../../img/emir.jpg")]
    private _onMouseOverListener: any;
    private _onMouseOutListener : any;

    constructor(innerRadius: number, outerRadius: number, thetaSegments: number, parameters : THREE.MeshBasicMaterialParameters ) {
        this._geometry = new THREE.RingGeometry(innerRadius, outerRadius, thetaSegments)
        this._material = new THREE.MeshBasicMaterial(parameters)
        this._ring = new THREE.Mesh(this._geometry, this._material);
        this._peoples = new Array();
        this.initPeople();
    }
    
    get geometry(): THREE.RingGeometry {
        return this._geometry;
    }

    get material(): THREE.MeshBasicMaterial {
        return this._material;
    }

    get mesh(): THREE.Mesh {
        return this._ring;
    }

    get peoples(): Array<People> {
        return this._peoples;
    }

    get onMouseOverListener(): any {
        return this._onMouseOverListener;
    }

    set onMouseOverListener(_onMouseOverListener: any) {
        this._onMouseOverListener = _onMouseOverListener;
    }

    get onMouseOutListener(): any {
        return this._onMouseOutListener;
    }

    set onMouseOutListener(_onMouseOutListener: any) {
        this._onMouseOutListener= _onMouseOutListener;
    }

    addPeoplesToRing(earth:Earth, group : THREE.Group) {
        this.peoples.forEach((people: People, index: number) => {
            people.mesh.position.set(this._geometry.parameters.outerRadius, this._ring.position.y - (index * .3), this._ring.position.z + .1)
            earth.mesh.add(people.mesh);
            group.add(people.mesh);
        })
    }

    removePeopleFromRing(earth:Earth, group : THREE.Group) {
        this.peoples.forEach((people: People, index: number) => {
            earth.mesh.remove(people.mesh);
            group.remove(people.mesh);
        })
    }

    initPeople() {
        this._peopleData.forEach((people:string) => {
            this.peoples.push(new People(.1, 32, {side: THREE.DoubleSide}, people));
        })
    }
}

export {
    Ring
}