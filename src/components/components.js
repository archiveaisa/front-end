import Vue from 'vue'

const requireComponent = require.context(
  // Look for files in the current directory
  '../components/',
  // Do not look in subdirectories
  true,
  // Only include "_base-" prefixed .ts files
  /[\w-]+\.component\.ts$/
)

// For each matching file name...
requireComponent.keys().forEach((fileName) => {
  // Get the component config
  const componentConfig = requireComponent(fileName)
  // Get the PascalCase version of the component name
  var prefix = "ayz-"
  const componentName = prefix.concat(fileName.match(/[\w-]+/));
  console.log("componentName ", componentName);


  // Globally register the component
  Vue.component(componentName, componentConfig.default || componentConfig)
})
