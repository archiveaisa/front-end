import Vue from "vue";
import VueRouter, { RouteConfig } from "vue-router";

/*COMPONENTS IMPORT*/
import HomePage from "./page/home/home-page"


Vue.use(VueRouter);


const accessToken = localStorage.getItem('access_token');
const routes: Array<RouteConfig> = [
  { path:'/', redirect:'/home'},
  { path: '/home', name: 'au-home-page', component: HomePage}
];

const router = new VueRouter({
  routes
});

router.beforeEach((to, from, next) => {
  const accessTokenUpdate = localStorage.getItem('access_token');
  if (to.matched.some(record => record.meta.requiresAuth)) {
    console.log('in beforeEach if requiresAuth', accessTokenUpdate);
    if (accessTokenUpdate == null) {
      console.log('in beforeEach if accessToken == null', accessTokenUpdate);
      next({
        path: '/back-office',
        params: { nextUrl: to.fullPath }
      })
    } else {
      console.log('in beforeEach else ', accessTokenUpdate);

      next()
    }
  } else if ((accessTokenUpdate !== null)) {
    console.log('in beforeEach else if accessToken !== null', accessTokenUpdate);
    next({
      path: '/admin',
      params: { nextUrl: to.fullPath }
    })
  } else {
    console.log('in beforeEach else if else', accessToken);
    next()
  }
  //next()
})

export default router;
