import Vue from "vue"
import { Component } from "vue-property-decorator";
import * as THREE from 'three';
import { Earth } from "../../model/geometry/earth/earth.geometry";
import { Ring } from "../../model/geometry/ring/ring.geometry";
declare var THREEx: any;

require('./home-page.scss')
@Component({
    name: "au-home-page",
    template: require("./home-page.html"),
})
export default class HomePage extends Vue {
    //intialize
    
    private scene : THREE.Scene;
    private axesHelper: THREE.AxesHelper;
    private light : THREE.SpotLight
    private camera: THREE.PerspectiveCamera;
    private renderer: THREE.WebGLRenderer;
    private domEvents : any;
    private earth: Earth;
    private id: number = -1;
    private angle : number = 0;
    
    constructor() {
        super();
        this.scene = new THREE.Scene();
        this.axesHelper = new THREE.AxesHelper(5);
        this.light = new THREE.SpotLight(0xffffff, 2, 10)
        this.camera = new THREE.PerspectiveCamera(75, window.innerWidth / window.innerHeight, 0.1, 1000);
        this.renderer = new THREE.WebGLRenderer();
        this.earth = new Earth()
        this.renderer.setSize(window.innerWidth, window.innerHeight);
        this.domEvents = new THREEx.DomEvents(this.camera, this.renderer.domElement);
        console.log('THREEx', THREEx)
    }

    mounted() {
        console.log('this.scene ', this.scene);
        document.getElementById('container')?.appendChild(this.renderer.domElement);
        window.addEventListener('resize', this.onWindowResize, false);
        this.scene.add(this.earth.group)
        this.camera.position.z = 3.3;
        this.light.position.set(-1.5, 1, 4);
        this.scene.add(this.light);
        this.earth.addRingsToEarth();
        this.scene.add(this.earth.mesh);
        this.earth.addRingsToGroup()
        this.addPeoplesToRingListener()
        this.animate();

    }


    addPeoplesToRingListener() {
        this.earth.rings.forEach((ring: Ring) => {
            this.domEvents.addEventListener(ring.mesh, 'mouseover', ring.onMouseOverListener = () => {
                this.earth.addPeoplesToRing(ring)
                this.renderFrame();
            }, false)

            this.domEvents.addEventListener(ring.mesh, 'mouseout', ring.onMouseOutListener = () => {
                this.earth.removePeoplesFromRing(ring)
                this.renderFrame();
            }, false)

            this.domEvents.addEventListener(ring.mesh, 'click',this.removePeoplesFromRingListener, false)
        })  
    }

    removePeoplesFromRingListener() {
        this.earth.rings.forEach((ring: Ring) => {
            this.domEvents.removeEventListener(ring.mesh, 'mouseover', ring.onMouseOverListener, false)
        }) 
    }

    animate() {
        this.id = requestAnimationFrame(this.animate);
        this.angle += .005;
        this.earth.group.rotation.z = -this.angle
        this.renderFrame()
    }

    renderFrame() {
        this.renderer.render(this.scene, this.camera)
    }

    onWindowResize() {
        this.camera.aspect = window.innerWidth / window.innerHeight;
        this.camera.updateProjectionMatrix();
        this.renderer.setSize(window.innerWidth, window.innerHeight);
        this.renderFrame()
    }



}   