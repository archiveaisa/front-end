import Vue from "vue";
import 'babel-polyfill';
import './components/components';
import vuetify  from './vuetify';
import 'vuetify/dist/vuetify.min.css'

import Axios from "axios";
import router from "./router.config";
import 'vuetify/dist/vuetify.min.css';
import 'material-design-icons-iconfont/dist/material-design-icons.css';
import store from "./store/store";
import colors from 'vuetify/es5/util/colors';
import { i18n } from "./i18n/i18n";
import validationMessagesEN from 'vee-validate/dist/locale/en';
import validationMessageFR from 'vee-validate/dist/locale/fr';

Axios.defaults.baseURL = "http://localhost:4000/";
Axios.defaults.responseType = "json";
if (localStorage.getItem('access_token')) {
  Axios.defaults.headers.common['Authorization'] = "Bearer " + localStorage.getItem('access_token');
}


let v = new Vue({ router, store, i18n, vuetify }).$mount('#app')
