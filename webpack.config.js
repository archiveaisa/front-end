var path = require('path');
var webpack = require('webpack');
const VueLoaderPlugin = require('vue-loader/lib/plugin')



module.exports = {
  mode: 'development',
  entry: [
    './src/app.ts'
  ],
  output: {
    path: path.resolve(__dirname, './dist'),
    publicPath: '/dist',
    filename: 'main.js'
  },
  module: {
   rules: [
     {
       test: /\.vue$/,
       loader: 'vue-loader',
       options: {
          loaders: {
            'scss': 'vue-style-loader!css-loader!sass-loader',
            'sass': 'vue-style-loader!css-loader!sass-loader?indentedSyntax',
          }
        }
     },
     {
       test: /\.tsx?$/,
       loader: 'ts-loader',
       exclude: /node_modules/,
       options: {
         appendTsSuffixTo: [/\.vue$/],
       }
     },
     {
       test: /\.(png|jpg|gif|svg|woff|woff2|eot|ttf)$/,
       loader: 'url-loader',
       options: {
         limit:10000,
         name: './assets/[hash].[ext]'
       }
     },
     {
       test: /\.html$/,
       loader: 'html-loader',
       options: {
         minimize: true
       }
     },
     {
        test: /\.scss$/,
        use: [
          'vue-style-loader',
          'css-loader',
          'sass-loader'
        ]
      },
      {
        test:/\.css$/,
        use:['style-loader','css-loader']
      }
   ]
 },
 plugins:[
    new VueLoaderPlugin()
 ],
 resolve: {
   extensions: ['.ts', '.js', '.vue', '.json'],
   alias: {
     'vue$': 'vue/dist/vue.esm.js'
   }
 },
 devServer: {
   historyApiFallback: true,
   noInfo: true
 },
 performance: {
   hints: false
 },
 devtool: '#eval-source-map'
}
