const fs = require('fs');
const jsonServer = require('json-server');
const jwt = require('jsonwebtoken');
const server = jsonServer.create();
const router = jsonServer.router('./fake-back-end/db.json');
const bodyParser = require('body-parser');
const profilesDB = JSON.parse(fs.readFileSync('./fake-back-end/profiles.json', 'UTF-8'));
const data_base = JSON.parse(fs.readFileSync('./fake-back-end/db.json', 'UTF-8'));
server.use(jsonServer.defaults());
server.use(bodyParser.urlencoded({extended: true}))
server.use(bodyParser.json())

const SECRET_KEY = '123456789';
const expiresIn = '1h'

// Create a token from a payload
function createToken(payload){
  return jwt.sign(payload, SECRET_KEY, {expiresIn})
}

// Verify the token
function verifyToken(token){
  return  jwt.verify(token, SECRET_KEY, (err, decode) => decode !== undefined ?  decode : err)
}

// Check if the user exists in database
function isAuthenticated(email, password){
  console.log('profilesDB.profiles => ', profilesDB.profiles);
  return profilesDB.profiles.findIndex(profile => profile.email === email && profile.password === password)
}

function getUser(indexProfile) {
  return data_base.user.find(user => user.id === profilesDB.profiles[indexProfile].user_id)
}

server.post('/auth/login', (req, res) => {
  const email = req.body.email;
  const password = req.body.password;
  var indexProfile = isAuthenticated(email, password);
  console.log('indexProfile => ', indexProfile);
  console.log('req.body => ',req.body);
  if ( indexProfile === -1) {
    console.log('isAuthenticated is false');
    const status = 401
    const message = 'Incorrect email or password'
    res.status(status).json({status, message})
    return
  }
  const access_token = createToken({email, password})
  const user = getUser(indexProfile);
  res.status(200).json({access_token, user})
})

server.use(/^(?!\/auth).*$/,  (req, res, next) => {
  // if (req.headers.authorization === undefined || req.headers.authorization.split(' ')[0] !== 'Bearer') {
  //   const status = 401
  //   const message = 'Bad authorization header'
  //   res.status(status).json({status, message})
  //   return
  // }
  // try {
  //    verifyToken(req.headers.authorization.split(' ')[1])
  //    next()
  // } catch (err) {
  //   const status = 401
  //   const message = 'Error: access_token is not valid'
  //   res.status(status).json({status, message})
  // }
  next();
})

server.use(router)

server.listen(4000, () => {
  console.log('Run Auth API Server')
})
